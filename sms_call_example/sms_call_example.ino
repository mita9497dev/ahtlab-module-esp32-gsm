#define MODEM_RX 16
#define MODEM_TX 17

#define gsm Serial2
bool started = false;

void setup() 
{
    Serial.begin(115200);
    GSM_Init();
    delay(1000);
    GSM_Call("0946866793");
    delay(10000);
    GSM_SendSMS("0946866793", "chào bạn");
}

void loop() 
{
    
}

void GSM_Init()
{
    started=true;
    gsm.begin(9600);
    delay(500);
}

void GSM_Call(String phoneNumber) {
    gsm.println("ATD" + phoneNumber + ";");
    DB_Serial(10000);
    gsm.println("ATH");
}

void GSM_SendSMS(String phoneNumber, String smsContent) {
    gsm.println(F("AT+CMGF=1"));
    DB_Serial(3000);
    gsm.println(F("AT+CSMP=17,167,2,25"));
    DB_Serial(3000);
    gsm.println(F("AT+CSCS=\"UCS2\""));
    DB_Serial(3000);
    GSM_SMS_Print_Phone(phoneNumber);
    DB_Serial(1000);
    GSM_SMS_Println(smsContent);
    gsm.write(0x1A);
    DB_Serial(3000);
}

void GSM_SMS_Print_Phone(String phoneNumber) {
    gsm.print(F("AT+CMGS=\""));
    unsigned char i=0;
    while(phoneNumber[i])
    {
        gsm.print(F("00"));
        gsm.print(phoneNumber[i],HEX);
        i++;
    }
    gsm.println(F("\""));
}

void GSM_SMS_Print(String data)
{
    unsigned char i=0;
    String buf="";
    while(data[i])
    {
        unsigned char c = data[i]&0xFF;
        if(c==0xE0)
        {
            gsm.print(F("0E"));
            Serial.print(F("OE"));
            i++;
            c = data[i];
            if(c == 0xB8)
            {
                i++;
                c = data[i]-0x80;
                if(c <= 0x0F)
                {
                  gsm.print(F("0"));
                  Serial.print(F("0"));        
                }
                buf = String(c,HEX);
                buf.toUpperCase();
                gsm.print(buf);
                Serial.print(buf); 
                }
            else
            {
                i++;
                c = data[i]-0x40;
                if(c <= 0x0F)
                {
                    gsm.print(F("0"));
                    Serial.print(F("0")); 
                }
                buf = String(c,HEX);
                buf.toUpperCase();
                gsm.print(buf);
                Serial.print(buf); 
            }     
        }
        else
        {
            gsm.print(F("00"));
            Serial.print(F("00")); 
            if(c == 0x0A)
            {
                gsm.print("0A");
                Serial.print(F("0A")); 
            }
            else if(c == 0x0D)
            {
                gsm.print("0D");
                Serial.print(F("0D")); 
            }
            else
            {
                buf = String(c,HEX);
                buf.toUpperCase();
                gsm.print(buf);
                Serial.print(buf); 
            }
        
        }
        i++;
    }
}

void GSM_SMS_Println(String data) {
    GSM_SMS_Print(data + "\r\n");
}

void DB_Serial(unsigned long timeout) 
{
    unsigned long previos = millis();
    while(millis() - previos < timeout) 
    {
        while(gsm.available()) 
        {
            Serial.write(gsm.read());
        }
    }
    Serial.println();
}
