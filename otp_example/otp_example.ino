/* ======================================== */
/* SIM800 */
/* ======================================== */

#define MODEM_RX 16
#define MODEM_TX 17

#define gsm Serial2


/* ======================================== */
/* WIFI */
/* ======================================== */
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#define     HOST            "www.mocky.io"
#define     PATH_LIST       "/v2/5e572d403000002b00fd3845"
#define     PATH_IDSMS      "/return-sms-otp"

#define SSID "AHTLAB"
#define PASS ""

WiFiClient client;
IPAddress dns(8,8,8,8);
String body;

void setup() 
{
    // put your setup code here, to run once:
    Serial.begin(115200);
    delay(1000);

    GSM_Init();
    WiFi_Init();
    delay(1000);
}

void loop() 
{
    bool requestStatus = requestGetApi(String("http://") + HOST + PATH_LIST);
    if(requestStatus)
    {
        Serial.println("request thanh cong");
        DynamicJsonDocument doc(400);
        DeserializationError error = deserializeJson(doc, body);
        if (error) 
        {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
            return;
        }
        if (doc.containsKey("data"))
        {
            uint8_t numSMS = doc["data"].size();
            for(int i = 0; i < numSMS; i++)
            {
                const char* id      = doc["data"][i]["sms_id"];
                const char* phone   = doc["data"][i]["sms_phone_receive"];
                const char* content = doc["data"][i]["sms_content"];
        
                Serial.print("id: ");
                Serial.println(id);
                Serial.print("phone: ");
                Serial.println(phone);
                Serial.print("content: ");
                Serial.println(content);

                // send sms
                bool smsStatus = GSM_SendSMS(String(phone), String(content));
                // Nếu gửi thành công
                if (smsStatus == 1) 
                {
                    Serial.println("Gui thanh cong");
//                    requestStatus = requestGetApi("http://" HOST PATH_IDSMS + String("/") + String(id));
//                    if (requestStatus)
//                    {
//                        Serial.println(body);
//                    }
                }
                else
                {
                    Serial.println("Gui that bai");
                }
                delay(50000);
            }
        }
    }
    Serial.println();
    delay(30000);
}

void GSM_Init() 
{
    gsm.begin(9600);
    delay(500);
}

void WiFi_Init()
{  
    WiFi.begin(SSID, PASS);
    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(500);
        Serial.print(".");
    }
    WiFi.config(WiFi.localIP(), WiFi.gatewayIP(), WiFi.subnetMask(), dns);

    Serial.println("WiFi connected");
}

bool requestGetApi(String api) 
{
    while (WiFi.status() != WL_CONNECTED) 
    {
        delay(500);
        Serial.print(".");
    }
//    Serial.println(api);
    
    body = "";
    HTTPClient http;
    http.begin(api);    
    delay(100);
    int httpResponseCode = http.GET();
    delay(1000);
    String payload = http.getString();
    Serial.print("response: ");
    Serial.println(payload);
    
    Serial.print("Http return code: ");
    Serial.println(httpResponseCode);  

    if(httpResponseCode == 200) 
    {
        Serial.println(F("wifi request success"));
        body = payload;
        Serial.println(F("Disconnect server"));
        
        http.end(); 
        return true;
    }
    else
    {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpResponseCode).c_str());
    }
    
    http.end();
    return false;
}

void GSM_SMS_Print_Phone(String phoneNumber) {
    gsm.print(F("AT+CMGS=\""));
    unsigned char i=0;
    while(phoneNumber[i])
    {
        gsm.print(F("00"));
        gsm.print(phoneNumber[i],HEX);
        i++;
    }
    gsm.println(F("\""));
}

bool GSM_SendSMS(String phoneNumber, String smsContent) {
    gsm.println(F("AT+CMGF=1"));
    debugSerial(3000);
    gsm.println(F("AT+CSCS=\"UCS2\""));
    debugSerial(3000);
    GSM_SMS_Print_Phone(phoneNumber);
    debugSerial(1000);
    gsm.print(utf8tohex(smsContent));
    gsm.write(0x1A);
    debugSerial(3000);
    
    return true;
}

String utf8tohex(String input) 
{
    String result;
    
    int len = input.length();
    for (int i = 0; i < len; i++) 
    {
        Serial.print("i: " + String(i) + " INT: ");
        Serial.print((uint16_t)input[i]);
        Serial.println(" HEX: " + String((uint16_t)input[i], HEX));
    }
    
    for(int i = 0; i < len; i++)
    {
        if ((uint16_t)input[i] >= 0xC3 && (uint16_t)input[i] < 0xE1) 
        {
            uint16_t val = 0xC0 + ((uint16_t)input[i] - 0xC3)*0x40 + ((uint16_t)input[i + 1] - 0x80);
            String valStr = String(val , HEX);
            result += valStr.length() == 2 ? "00" + valStr : "0" + valStr;
            ++i;
        } 
        else if((uint16_t)input[i] == 0xE1 && (uint16_t)input[i + 1] >= 0xB8) 
        {
            result += "1e" + String(((uint16_t)input[i + 1] - 0xB8)*0x40 + (uint16_t)input[i + 2] - 0x80, HEX);
            i += 2;
        } 
        else 
        {
            result += "00";
            result += String((uint16_t)input[i], HEX); 
        }
    }
    
    return result;
}

void debugSerial(unsigned long timeout) 
{
  unsigned long previos = millis();
  while(millis() - previos < timeout) 
  {
    while(gsm.available()) 
    {
      Serial.write(gsm.read());
    }
  }
  Serial.println();
}
