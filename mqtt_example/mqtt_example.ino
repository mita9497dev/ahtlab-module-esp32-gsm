#include <WiFi.h>
#include <PubSubClient.h>

const char ssid[] = "AHTLAB";
const char pass[] = "";

#define MQTT_SERVER "postman.cloudmqtt.com"
#define MQTT_PORT 17881
#define MQTT_USER "zxyndpfc"
#define MQTT_PASS "X2fbxwwciAbu"

WiFiClient net;
PubSubClient client(net);
unsigned long lastTimePublish = 0;

const char* subTopic = "/ahtlab/sub";
const char* pubTopic = "/ahtlab/pub";
const char* clientId = "ahtlab_javis_01";

void setup()
{
    Serial.begin(115200);
    delay(1000);

    WiFi_Init();
    MQTT_Init();
    MQTT_Connect();
}

void loop()
{
    client.loop();
    delay(10);
    
    if (!client.connected()) 
    {
        MQTT_Connect();
    }

    if (millis() - lastTimePublish > 30000L) 
    {
        MQTT_PublishMessage(pubTopic, "bb i'm here");
        lastTimePublish = millis();
    }
}

void WiFi_Init() 
{
    WiFi.begin(ssid, pass);
    Serial.print("wifi connecting...");
}

void WiFi_CheckConnect()
{
    while (WiFi.status() != WL_CONNECTED) 
    {
        Serial.print(".");
        delay(1000);
    }
}

void MQTT_Init()
{
    client.setServer(MQTT_SERVER, MQTT_PORT);
    client.setCallback(MQTT_OnMessage);
}

void MQTT_Connect()
{
    WiFi_CheckConnect();
    Serial.print("\nMQTT connecting...");
    while (!client.connect(clientId, "zxyndpfc", "X2fbxwwciAbu")) 
    {
        Serial.print(".");
        delay(1000);
    }
    Serial.println("nMQTT connected!");
    client.subscribe(subTopic);
}

void MQTT_OnMessage(char* topic, byte* payload, unsigned int length)
{
    Serial.println("-------new message from broker-----");
    Serial.print("topic:");
    Serial.println(topic);
    Serial.print("data:");  
    Serial.write(payload, length);
    Serial.println();
}

void MQTT_PublishMessage(const char* topic, const char* message)
{
    if (!client.connected()) 
    {
        MQTT_Connect();
    }
    client.publish(topic, message);
}
