/* ======================================== */
/* SIM800 */
/* ======================================== */
#include <aht_sim800.h>
#define MODEM_RX 16
#define MODEM_TX 17
#define uart Serial2

AHT_GSM *gsmMaster = new AHT_GSM(&uart);
AHT_GSM *gsm;
GSM_TYPE gsmType;
bool gsmOk = false;

const char* phoneBook[] = {"0941732379", "0941732379"};
const uint8_t phoneBookLen = 2;

/* ======================================== */
/* BTN */
/* ======================================== */
#define BTN_PIN 2
#define PRESS_STATE 0

void setup() 
{
    Serial.begin(115200);
    delay(1000);

    if(gsmMaster->begin()) 
    {
        gsmType = gsmMaster->detectGSM(&uart);
        unsigned long baudrate = gsmMaster->getBaudrate();
        free(gsmMaster);
        
        if(gsmType == SIM800) 
        {
            gsm = new AHT_SIM800(&uart);
            gsm->begin(baudrate);

            gsm->println("AT&FZ");
            if (gsm->readResponse(1000, "OK") == AT_REPLY_FOUND)
            {
                gsmOk = true;
            }
        }

        delay(5000);
    }
}

void loop()
{
    if(!gsmOk)
    {
        Serial.println("can't detect gsm module");
        delay(5000);
        return;
    }

    if (BTN_Read(BTN_PIN))
    {
        for (uint8_t i = 0; i < phoneBookLen; i++)
        {
            Serial.print(F("phone: "));
            Serial.println(phoneBook[i]);

            if(gsm->sendSMS(phoneBook[i], "cafe5hsang - mita9497dev"))
            {
                Serial.println(F("Gui SMS thanh cong"));
            }
        }
    }

    delay(50);
}

void BTN_Init()
{
    pinMode(BTN_PIN, INPUT);
}

bool BTN_Read(uint8_t pin)
{
    return readDigital(pin, PRESS_STATE);
}

bool readDigital(uint8_t pin, bool state)
{
    if (digitalRead(pin) == state)
    {
        delay(50);
        if (digitalRead(pin) == state)
        {
            return true;
        }
    }
    return false;
}
