#include "SPIFFS.h"

/* ========================== */
/* BOARD SETTINGS */
/* ========================== */

#define BTN 5
#define BTN_PRESS HIGH

/* ========================== */
/* WiFiManager */
/* ========================== */
#include <ArduinoJson.h>            //https://github.com/bblanchon/ArduinoJson
#include <WiFi.h>
#include <WebServer.h>
#include <WiFiManager.h>            //local folder

#define CONFIG_WIFI_FAIL 0

unsigned long configWaitTime = 15000L;

/* ========================== */
/* MQTT */
/* ========================== */
#include <PubSubClient.h>

char MQTT_SERVER[50] = "postman.cloudmqtt.com";
char MQTT_PORT[6] = "17881";
char MQTT_USER[30] = "zxyndpfc";
char MQTT_PASS[30] = "X2fbxwwciAbu";

const char* subTopic = "/ahtlab/sub";
const char* pubTopic = "/ahtlab/pub";
const char* clientId = "ahtlab_javis_01";

WiFiClient net;
PubSubClient client(net);
unsigned long lastTimePublish = 0;

void setup()
{
    Serial.begin(115200);
    delay(100);

    pinMode(BTN, INPUT);

    SPIFFS.begin(true);

    Config_Load();

    WiFi_Init();
    MQTT_Init();
    MQTT_Connect();
}

void loop()
{
    client.loop();
    delay(10);
    
    if (!client.connected()) 
    {
        MQTT_Connect();
    }

    if (millis() - lastTimePublish > 30000L) 
    {
        MQTT_PublishMessage(pubTopic, "bb i'm here");
        lastTimePublish = millis();
    }
}

void ESP_Yeild()
{
    delay(1);
}

bool BTN_Read(int btn)
{
    if (digitalRead(BTN) == BTN_PRESS)
    {
        delay(50);
        if (digitalRead(BTN) == BTN_PRESS)
        {
            return true;
        }
    }

    return false;
}

void Config_Wait()
{
    Serial.println(F("Wait config"));
    
    unsigned long t = millis();
    while(millis() - t < configWaitTime)
    {
        if (BTN_Read(BTN))
        {
            Config();
        }

        ESP_Yeild();
    }
}

void Config_Load()
{
    if (SPIFFS.exists("/config.json")) 
    {
        Serial.println(F("reading config file"));
        File configFile = SPIFFS.open("/config.json", "r");
        
        if (configFile) 
        {
            Serial.println(F("opened config file"));
            size_t size = configFile.size();

            std::unique_ptr < char[] > buf(new char[size]);
            configFile.readBytes(buf.get(), size);
            
            StaticJsonDocument<512> json;
            DeserializationError error = deserializeJson(json, buf.get());

            if (!error) 
            {
                Serial.println(F("\nparsed json"));
    
                strcpy(MQTT_SERVER, json["mqtt_server"]);
                strcpy(MQTT_PORT, json["mqtt_port"]);
                strcpy(MQTT_USER, json["mqtt_user"]);
                strcpy(MQTT_PASS, json["mqtt_pass"]);

                Serial.println(MQTT_SERVER);
                Serial.println(MQTT_PORT);
                Serial.println(MQTT_USER);
                Serial.println(MQTT_PASS);
            } 
            else 
            {
                Serial.println(F("Failed to load json config"));
            }
            configFile.close();
        }

        Config_Wait();
    }
    else
    {
        Serial.println(F("Config"));
        Config();
    }
}

void Config()
{
    WiFiManagerParameter custom_mqtt_server("server", "mqtt server", MQTT_SERVER, 40);
    WiFiManagerParameter custom_mqtt_port("port", "mqtt port", MQTT_PORT, 6);
    WiFiManagerParameter custom_mqtt_user("user", "mqtt user", MQTT_USER, 30);
    WiFiManagerParameter custom_mqtt_pass("pass", "mqtt pass", MQTT_PASS, 30);

    WiFiManager wifiManager;
    wifiManager.setBreakAfterConfig(true);
    wifiManager.setSaveConfigCallback(Config_Callback);
    
    //add all your parameters here
    wifiManager.addParameter(&custom_mqtt_server);
    wifiManager.addParameter(&custom_mqtt_port);
    wifiManager.addParameter(&custom_mqtt_user);
    wifiManager.addParameter(&custom_mqtt_pass);

    if (!wifiManager.startConfigPortal("AHTLAB")) 
    {
        Serial.println(F("[CONFIG] failed to connect and hit timeout"));
        delay(3000);

        ESP.restart();
        delay(5000);
    }

    Serial.println(F("[CONFIG] connected"));

    strcpy(MQTT_SERVER, custom_mqtt_server.getValue());
    strcpy(MQTT_PORT, custom_mqtt_port.getValue());
    strcpy(MQTT_USER, custom_mqtt_user.getValue());
    strcpy(MQTT_PASS, custom_mqtt_pass.getValue());

    Serial.println(F("[CONFIG] Local ip"));
    Serial.println(WiFi.localIP());
}

void Config_Callback()
{
    Serial.println(F("[CONFIG] Callback"));
    Serial.println(F("[CONFIG] saving config"));
        
    StaticJsonDocument<512> json;
    
    json["mqtt_server"] = MQTT_SERVER;
    json["mqtt_port"] = MQTT_PORT;
    json["mqtt_user"] = MQTT_USER;
    json["mqtt_pass"] = MQTT_PASS;

    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) 
    {
        Serial.println(F("[CONFIG] failed to open config file for writing"));
    }

    if (serializeJson(json, configFile) == 0) 
    {
        Serial.println(F("Failed to write to file"));
    }
    configFile.close();

    ESP.restart();
}

void WiFi_Init() 
{
//    WiFi.begin(ssid, pass);
    if (CONFIG_WIFI_FAIL)
    {
        uint8_t time = 0;
        while (++time < 5) 
        {
            if (WiFi.status() == WL_CONNECTED)
            {
                return;
            }
            Serial.print(".");
            delay(1000);
        }

        Config();
    }
    Serial.print("wifi connecting...");
}

void WiFi_CheckConnect()
{
    while (WiFi.status() != WL_CONNECTED) 
    {
        Serial.print(".");
        delay(1000);
    }
}

void MQTT_Init()
{
    client.setServer(MQTT_SERVER, String(MQTT_PORT).toInt());
    client.setCallback(MQTT_OnMessage);
}

void MQTT_Connect()
{
    WiFi_CheckConnect();
    Serial.print("\nMQTT connecting...");
    while (!client.connect(clientId, MQTT_USER, MQTT_PASS)) 
    {
        Serial.print(".");
        delay(1000);
    }
    Serial.println("nMQTT connected!");
    client.subscribe(subTopic);
}

void MQTT_OnMessage(char* topic, byte* payload, unsigned int length)
{
    Serial.println("-------new message from broker-----");
    Serial.print("topic:");
    Serial.println(topic);
    Serial.print("data:");  
    Serial.write(payload, length);
    Serial.println();
}

void MQTT_PublishMessage(const char* topic, const char* message)
{
    if (!client.connected()) 
    {
        MQTT_Connect();
    }
    client.publish(topic, message);
}
