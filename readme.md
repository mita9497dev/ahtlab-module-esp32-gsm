
# Thư viện
- MQTT Client: [https://github.com/256dpi/arduino-mqtt](https://github.com/256dpi/arduino-mqtt)
- PubSubClient: [https://github.com/knolleary/pubsubclient](https://github.com/knolleary/pubsubclient)
- WiFiManager: [https://github.com/tzapu/WiFiManager](https://github.com/tzapu/WiFiManager)
- ArduinoJson (Version 6): [https://github.com/bblanchon/ArduinoJson](https://github.com/bblanchon/ArduinoJson)
# Công cụ
- MQTTLens chrome extension: [https://chrome.google.com/webstore/detail/mqttlens/hemojaaeigabkbcookmlgmdigohjobjm?hl=vi](https://chrome.google.com/webstore/detail/mqttlens/hemojaaeigabkbcookmlgmdigohjobjm?hl=vi)

# Hướng dẫn
#### mqtt_smartconfig_example
 - Thông tin MQTT server (Account, Test) có trong code, Hướng dẫn test xem ví dụ **mqtt_example** bên dưới
 - Smart Config:
    - Sử dụng thư viện WiFiManager, ArduinoJson (xem mục **Thư viện**)
    - Trường hợp Smart Config:
       - Thiết bị chưa setup lần nào sẽ vào thẳng smart config
       - 15 giây đầu sau khi khởi động thiết bị sẽ chờ nhấn nút config, nếu không sẽ không chạy smart config
       - **Nếu không kết nối được với WiFi** (Do sai pass, do wifi):
          - Kết nối WiFi không thành công **vào** smart config:
          ```c++
            #define CONFIG_WIFI_FAIL 1
          ```
          - Kết nối WiFi không thành công **không vào** smart config (mặc định):
          ```c++
			#define CONFIG_WIFI_FAIL 0
          ```

#### sms_call_example
 - ESP32 và SIM800
 - Gọi điện, nhắn tin

#### mqtt_example
 - ESP32 và SIM800
 - Sử dụng server: [https://www.cloudmqtt.com/](https://www.cloudmqtt.com/)
 - Test bằng MQTTLens

#### mqtt_sms_call_example
- ESP32 và SIM800 
- ESP8266 và SIM800 
- Kết hợp mqtt, sms, call
