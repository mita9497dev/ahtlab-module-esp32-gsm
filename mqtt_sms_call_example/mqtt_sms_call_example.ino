/* ==================== */
/* WIFI */ 
/* ==================== */
#ifdef ESP32
#include <WiFi.h>
/* GSM */ 
#define MODEM_RX 16
#define MODEM_TX 17
#define gsm Serial2
#endif

#ifdef ESP8266
#include <ESP8266WiFi.h>
/* GSM */ 
#include <SoftwareSerial.h>
int _GSM_RXPIN_ = 13;
int _GSM_TXPIN_ = 15;
SoftwareSerial gsm(_GSM_RXPIN_, _GSM_TXPIN_);
#endif

#include <PubSubClient.h>

const char ssid[] = "AN BINH";
const char pass[] = "anbinhbio.com";

#define MQTT_SERVER "postman.cloudmqtt.com"
#define MQTT_PORT 17881
#define MQTT_USER "zxyndpfc"
#define MQTT_PASS "X2fbxwwciAbu"

WiFiClient net;
PubSubClient client(net);
unsigned long lastTimePublish = 0;

const char* subTopic = "/ahtlab/sub";
const char* pubTopic = "/ahtlab/pub";
const char* clientId = "ahtlab_javis_01";

bool started = false;
void setup()
{
    Serial.begin(115200);
    delay(1000);

    pinMode(5, INPUT);

    WiFi_Init();
    
//    GSM_Init();
//    MQTT_Init();
//    MQTT_Connect();

//    GSM_Call("0946866793");
//    delay(10000);
//    GSM_SendSMS("0946866793", "chào bạn");

    
}

void loop()
{
    Serial.println(digitalRead(5));
//    client.loop();
//    delay(10);
//    
//    if (!client.connected()) 
//    {
//        MQTT_Connect();
//    }
//
//    if (millis() - lastTimePublish > 30000L) 
//    {
//        MQTT_PublishMessage(pubTopic, "bb i'm here");
//        lastTimePublish = millis();
//    }
}

void WiFi_Init() 
{
    WiFi.begin(ssid, pass);
    Serial.print("wifi connecting...");
    delay(1000);
}

void WiFi_CheckConnect()
{
    while (WiFi.status() != WL_CONNECTED) 
    {
        Serial.print(".");
        delay(1000);
    }
}

void MQTT_Init()
{
    client.setServer(MQTT_SERVER, MQTT_PORT);
    client.setCallback(MQTT_OnMessage);
    delay(100);
}

void MQTT_Connect()
{
    WiFi_CheckConnect();
    Serial.print("\nMQTT connecting...");
    while (!client.connect(clientId, "zxyndpfc", "X2fbxwwciAbu")) 
    {
        Serial.print(".");
        delay(1000);
    }
    Serial.println("nMQTT connected!");
    client.subscribe(subTopic);
}

void MQTT_OnMessage(char* topic, byte* payload, unsigned int length)
{
    Serial.println("-------new message from broker-----");
    Serial.print("topic:");
    Serial.println(topic);
    Serial.print("data:");  
    Serial.write(payload, length);
    Serial.println();
}

void MQTT_PublishMessage(const char* topic, const char* message)
{
    if (!client.connected()) 
    {
        MQTT_Connect();
    }
    client.publish(topic, message);
    delay(100);
}

void GSM_Init()
{
    started=true;
    gsm.begin(9600);
    delay(1000);
}

void GSM_Call(String phoneNumber) {
    gsm.println("ATD" + phoneNumber + ";");
    DB_Serial(10000);
    gsm.println("ATH");
}

void GSM_SendSMS(String phoneNumber, String smsContent) {
    gsm.println(F("AT+CMGF=1"));
    DB_Serial(3000);
    gsm.println(F("AT+CSCS=\"UCS2\""));
    DB_Serial(3000);
    GSM_SMS_Print_Phone(phoneNumber);
    DB_Serial(1000);
    GSM_SMS_Println(smsContent);
    gsm.write(0x1A);
    DB_Serial(3000);
}

void GSM_SMS_Print_Phone(String phoneNumber) {
    gsm.print(F("AT+CMGS=\""));
    unsigned char i=0;
    while(phoneNumber[i])
    {
        gsm.print(F("00"));
        gsm.print(phoneNumber[i],HEX);
        i++;
    }
    gsm.println(F("\""));
}

void GSM_SMS_Print(String data)
{
    unsigned char i=0;
    String buf="";
    while(data[i])
    {
        unsigned char c = data[i]&0xFF;
        if(c==0xE0)
        {
            gsm.print(F("0E"));
            Serial.print(F("OE"));
            i++;
            c = data[i];
            if(c == 0xB8)
            {
                i++;
                c = data[i]-0x80;
                if(c <= 0x0F)
                {
                  gsm.print(F("0"));
                  Serial.print(F("0"));        
                }
                buf = String(c,HEX);
                buf.toUpperCase();
                gsm.print(buf);
                Serial.print(buf); 
                }
            else
            {
                i++;
                c = data[i]-0x40;
                if(c <= 0x0F)
                {
                    gsm.print(F("0"));
                    Serial.print(F("0")); 
                }
                buf = String(c,HEX);
                buf.toUpperCase();
                gsm.print(buf);
                Serial.print(buf); 
            }     
        }
        else
        {
            gsm.print(F("00"));
            Serial.print(F("00")); 
            if(c == 0x0A)
            {
                gsm.print("0A");
                Serial.print(F("0A")); 
            }
            else if(c == 0x0D)
            {
                gsm.print("0D");
                Serial.print(F("0D")); 
            }
            else
            {
                buf = String(c,HEX);
                buf.toUpperCase();
                gsm.print(buf);
                Serial.print(buf); 
            }
        
        }
        i++;
    }
}

void GSM_SMS_Println(String data) {
    GSM_SMS_Print(data + "\r\n");
}

void DB_Serial(unsigned long timeout) 
{
    unsigned long previos = millis();
    while(millis() - previos < timeout) 
    {
        while(gsm.available()) 
        {
            Serial.write(gsm.read());
        }
    }
    Serial.println();
}
