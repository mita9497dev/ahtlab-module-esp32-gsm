/* ======================================== */
/* SIM800 */
/* ======================================== */

#define MODEM_RX 16
#define MODEM_TX 17

#define gsm Serial2


/* ======================================== */
/* WIFI */
/* ======================================== */
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>

#define SSID "AHTLAB"
#define PASS ""

WiFiClient client;
IPAddress dns(8,8,8,8);
String body;

#define     URI     "http://ibsmanage.com"
#define     PATTERN "%s/Active?IDImeiSim=%s&IDImeiDevice=%s&gen=%s&Device=%s&IDPartner=%d&t=%d"
char URL[120];
char IMEI_SIM[20]   = "452049921228394";
char IMEI_GSM[20]   = "12345678910";
char GEN[]          = "2G";
char DEVICE_TYPE[]  = "ESP32SIM800A";
char ID_PARTNER[]   = "IDPartner";
int t = 0;

void setup() 
{
    // put your setup code here, to run once:
    Serial.begin(115200);
    delay(1000);

    GSM_Init();
    WiFi_Init();
    delay(1000);
}

void loop() 
{
    requestActive();
    delay(10000);
}

void requestActive()
{
    sprintf(URL, PATTERN, URI, IMEI_SIM, IMEI_GSM, GEN, DEVICE_TYPE, ID_PARTNER);
    Serial.println(URL);

    bool requestStatus = getRequest(String(URL));
    if(requestStatus)
    {
        Serial.println("Request thanh cong");
        Serial.print("body: ");
        Serial.println(body);
    }
    Serial.println();
}

void GSM_Init() 
{
    gsm.begin(9600);
    delay(500);
}

void WiFi_Init()
{  
    WiFi.begin(SSID, PASS);
    int timeConnect = 0;
    while (WiFi.status() != WL_CONNECTED && timeConnect++ < 10) 
    {
        delay(500);
        Serial.print(".");
    }
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial.println("WiFi not connected");
        return;
    }
    Serial.println("WiFi connected");
    WiFi.config(WiFi.localIP(), WiFi.gatewayIP(), WiFi.subnetMask(), dns);
}

bool getRequest(String api)
{
    if (WiFi.status() != WL_CONNECTED)
    {
        Serial.println("WL_NOT_CONNECTED: GPRS Request");
        return GSM_GetRequest(api);
    }
    else
    {
        Serial.println("WL_CONNECTED: WIFI Request");
        return WiFi_GetRequest(api);
    }
}

bool WiFi_GetRequest(String api) 
{
    body = "";
    HTTPClient http;
    http.begin(api);    
    delay(100);
    int httpResponseCode = http.GET();
    delay(1000);
    String payload = http.getString();
    Serial.print("response: ");
    Serial.println(payload);
    
    Serial.print("Http return code: ");
    Serial.println(httpResponseCode);  

    if(httpResponseCode == 200) 
    {
        Serial.println(F("wifi request success"));
        body = payload;
        Serial.println(F("Disconnect server"));
        
        http.end(); 
        return true;
    }
    else
    {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpResponseCode).c_str());
    }
    
    http.end();
    return false;
}

bool GSM_GetRequest(String api)
{
    body = "";
    
    gsm.println("AT+CGATT=1");
    delay(200);
    GSM_ClearBuffer();
    
    gsm.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"");//setting the SAPBR,connection type is GPRS
    delay(1000);
    GSM_ClearBuffer();
    
    gsm.println("AT+SAPBR=3,1,\"APN\",\"\"");//setting the APN,2nd parameter empty works for all networks 
    delay(5000);
    GSM_ClearBuffer();
    
    gsm.println();
    gsm.println("AT+SAPBR=1,1");
    delay(10000);
    GSM_ClearBuffer();
    
    gsm.println("AT+HTTPINIT"); //init the HTTP request
    delay(2000); 
    GSM_ClearBuffer();
    
    gsm.println("AT+HTTPPARA=\"URL\"," + api);// setting the httppara, 
    //the second parameter is the website from where you want to access data 
    delay(1000);
    GSM_ClearBuffer();
    
    gsm.println();
    gsm.println("AT+HTTPACTION=0");//submit the GET request 
    delay(8000);//the delay is important if the return datas are very large, the time required longer.

    unsigned long previous = millis();
    while(millis() - previous < 5000L) 
    {
        delay(1);
        while(gsm.available()!= 0) 
        {
            body += (char)gsm.read();
        }  
    }
    
    Serial.println("response:" + body);
    if(body.indexOf("404") == -1) 
    {
        gsm.println("AT+HTTPREAD");// read the data from the website you access
        delay(3000);

        body = "";
        previous = millis();
        while(millis() - previous < 5000L) 
        {
            delay(1);
            while(gsm.available()!= 0) 
            {
                body += (char)gsm.read();
            }  
        }
        Serial.print("body: ");
        Serial.println(body);
        
        delay(1000);
        gsm.println("AT+HTTPTERM");// terminate HTTP service
        GSM_ClearBuffer();
        Serial.println(F("GPRS success"));
        
        return true;
    } 
    
    gsm.println("");
    delay(1000);
    gsm.println("AT+HTTPTERM");// terminate HTTP service
    GSM_ClearBuffer(); 
    Serial.println(F("GPRS failed"));
    
    return false;
}

void GSM_ClearBuffer()
{
    while (gsm.available()) 
    {
        Serial.write(gsm.read());
    } 
    
    while (Serial.available()) 
    {
        gsm.write(Serial.read());
    }
}
